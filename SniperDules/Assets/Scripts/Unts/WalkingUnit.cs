﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingUnit : Unit
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private int tileSpeed;
    
    public bool selected;
    public bool hasMoved;
    public PlayCard playCard;
    
    public Vector2 futurePosition;
    
    public override void Start()
    {
        base.Start();
        playCard = FindObjectOfType<PlayCard>();
        futurePosition = transform.position;
    }

    public void UnitAction()
    {
        StartCoroutine(StartAction(futurePosition));
    }

    private void OnMouseDown()
    {
        if (isTargetable)
        {
            cardJustPlayed.GetChosenUnit(this);
            playCard.PrepareCardToPlay(cardJustPlayed);
            GameState.Instance.ResetTilesAndUnits();
        }
        else if (!playCard.IsPlayerChoosingTile)
        {
            SelectingUnit();
        }
    }

    private void SelectingUnit()
    {
        if (selected)
        {
            selected = false;
            GameState.Instance.selectedUnit = null;
            GameState.Instance.ResetTilesAndUnits();
        }
        else
        {
            if (team == GameState.Instance.activeTeam)
            {
                if (GameState.Instance.selectedUnit != null)
                {
                    GameState.Instance.selectedUnit.selected = false;
                    GameState.Instance.ResetTilesAndUnits();
                }
                selected = true;
                GameState.Instance.selectedUnit = this;
                
                GetWalkableTiles();
            }
        }
    }
    
    public void SetFuturePosition(Vector2 futurePositionToSet)
    {
        futurePosition = futurePositionToSet;
        hasMoved = true;
    }
    public void GetWalkableTiles()
    {
        if (hasMoved) return;
        TileManager.Instance.HighlightWalkableTiles(transform.position, tileSpeed);
    }

    protected virtual IEnumerator StartAction(Vector2 tilePos)
    {
        while (transform.position.x != tilePos.x)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(tilePos.x, transform.position.y), moveSpeed * Time.deltaTime);
            yield return null;
        }

        while (transform.position.y != tilePos.y)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, tilePos.y), moveSpeed * Time.deltaTime);
            yield return null;
        }
    }

}
