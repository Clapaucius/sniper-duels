﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;



public class UiCollectionManager : MonoBehaviour
{
    public TextMeshProUGUI playerBanner;
    public DeckDisplay[] deckDisplays;
    public CardDisplay[] cardDisplays;
    public List<Card> listOfAllCards = new List<Card>();
    
    public Canvas deckEditingCanvas;
    public Canvas deckChoosingCanvas;

    public GameObject previousPageButton;
    public GameObject nextPageButton;

    int _pageNumber;
    int _maxPageNumber;

    public static UiCollectionManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _maxPageNumber = (int)Mathf.Ceil(listOfAllCards.Count / 6f) - 1;
        SetCardsInCollection();
    }


    public void SetBanner(PlayerInCollection player)
    {
        switch (player)
        {
            case PlayerInCollection.GreenPlayer:
                playerBanner.text = "Колоды Зеленого игрока:";
                break;

            case PlayerInCollection.RedPlayer:
                playerBanner.text = "Колоды Красного игрока:";
                break;
        }
    }

    public void SetDecks(Deck[] decks)
    {
        for (int i = 0; i < deckDisplays.Length; i++)
        {
            if (decks[i] != null)
            {
                deckDisplays[i].SetDeck(deckDisplays[i], decks[i]);
            }
        }
    }

    void SetCardsInCollection()
    {
        for (var i = 0; i < cardDisplays.Length; i++)
        {
            int n = i + _pageNumber * cardDisplays.Length;

            if (n < listOfAllCards.Count)
            {
                cardDisplays[i].gameObject.SetActive(true);
                cardDisplays[i].SetCardUi(listOfAllCards[n]);
            }
            else
            {
                cardDisplays[i].gameObject.SetActive(false);
            }
        }

        previousPageButton.SetActive(_pageNumber != 0);

        nextPageButton.SetActive(_pageNumber != _maxPageNumber);

        CheckAllCardDisplays();
    }


    public void CheckAllCardDisplays()
    {
        foreach (var cardDisplay in cardDisplays)
        {
            if (CollectionManager.Instance.IsThisCardInDeckAlready(cardDisplay.myCard))
            {
                cardDisplay.LockCardDisplay();
            }
            else
            {
                cardDisplay.UnlockCardDisplay();
            }
        }
    }

    public void ChangeNameOfDeck()
    {

    }

    public void ShowPreviousPage()
    {
        _pageNumber--;
        SetCardsInCollection();
    }

    public void ShowNextPage()
    {
        _pageNumber++;
        SetCardsInCollection();
    }
}
