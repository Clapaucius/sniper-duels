﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Serialization;


public class UiManager : MonoBehaviour
{
    public GameObject startTurnButton;
    public GameObject endTurnButton;
    public CardDisplay[] mainCardDisplays;

    public CardDisplay[] firstPreGameCardDisplays;
    public CardDisplay[] secondPreGameCardDisplays;
    public CardDisplay[] thirdPGameCardDisplays;

    [FormerlySerializedAs("preGameUIArray")] public GameObject[] preGameUiArray = new GameObject[3];
    
    public Canvas mainCanvas;

    public Button preGameButton;


    public TextMeshProUGUI playerBanner;
    public TextMeshProUGUI scoreBanner;

    public Canvas deathScreenCanvas;
    public TextMeshProUGUI whoWonText;
    public Button deathScreenButton;

    private int _handSize;

    public static UiManager Instance
    {
        get;
        private set;
    }
    private void Awake()
    {
        Instance = this;
        _handSize = GameState.Instance.redPlayer.myTempHand.Count;
    }

    private void Start()
    {
        DisableCardsUi();

        var tempColor = new Color(255, 255, 255, 0.2f);
        preGameButton.gameObject.GetComponent<Image>().color = tempColor;
        preGameButton.enabled = false;

        endTurnButton.SetActive(false);
        ChangeBannerAndButtons(false);
    }

    public void SetScore()
    {
        scoreBanner.text = Score();
    }

    private string Score()
    {
        return $"<color=#C80000>{GameState.NumberOfGreenPlayerDeaths}</color> : <color=#1CA800>{GameState.NumberOfRedPlayerDeaths}</color>";

    }
  
    public void EnableDeathScreen(Player winner)
    {
        mainCanvas.enabled = false;
        deathScreenCanvas.enabled = true;

        if (winner == GameState.Instance.greenPlayer)
        {
            switch (GameState.NumberOfRedPlayerDeaths)
            {
                case 1:
                    whoWonText.text = $"Зеленый побеждает в этом раунде! Счет {Score()}";
                    deathScreenButton.GetComponentInChildren<TextMeshProUGUI>().text = "Следующий раунд!";
                    deathScreenButton.onClick.AddListener(GameState.Instance.ReloadScene);
                    break;
                case 2:
                    whoWonText.text = $"Зеленый одерживает победу со счетом {Score()}!";
                    deathScreenButton.GetComponentInChildren<TextMeshProUGUI>().text = "В меню!";
                    deathScreenButton.onClick.AddListener(GameState.Instance.ReturnToMenu);
                    break;
            }
        }
        else if (winner == GameState.Instance.redPlayer)
        {
            Debug.Log(winner);
            if (GameState.NumberOfGreenPlayerDeaths == 1)
            {
                whoWonText.text = $"Красный побеждает в этом раунде! Счет {Score()}";
                deathScreenButton.GetComponentInChildren<TextMeshProUGUI>().text = "Следующий раунд!";
                deathScreenButton.onClick.AddListener(GameState.Instance.ReloadScene);
            }
            else if (GameState.NumberOfGreenPlayerDeaths == 2)
            {
                whoWonText.text = $"Красный одерживает победу со счетом {Score()}!";
                deathScreenButton.GetComponentInChildren<TextMeshProUGUI>().text = "В меню!";
                deathScreenButton.onClick.AddListener(GameState.Instance.ReturnToMenu);
            }
        }
    }

    public void EnableCardChoosingUi(CardDisplay[] cardDisplays)
    {
        
        //в первом раунде от 0 до 0; во втором и третьем от 0 до 2
        for (int i = 0; i < _handSize; i++)
        {
            var myButton = cardDisplays[i].GetComponentInChildren<Button>();

            CardDisplay x = cardDisplays[i];
            myButton.onClick.AddListener(delegate { RemoveFromHandOnPress(x, cardDisplays); });
        }

        //в первом раунде от 0 до 4; во втором от 3 до 4; в третьем от 3 до 3
        for (int i = _handSize; i < cardDisplays.Length; i++)
        {
            Button myButton = cardDisplays[i].GetComponentInChildren<Button>();  //пусть будет

            CardDisplay x = cardDisplays[i];
            myButton.onClick.AddListener(delegate {AddToHandOnPress(x, cardDisplays); });
        }

    }

    public void SetCardsToChoose(GameObject currentPreGameUi, CardDisplay[] cardDisplays, Card[] cardsToSet)
    {
        mainCanvas.enabled = false;
        preGameButton.gameObject.SetActive(true);
        currentPreGameUi.SetActive(true);

        for (int i = 0; i < cardsToSet.Length; i++)
        {
            cardDisplays[i].SetCardUi(cardsToSet[i]);
        }
    }

    public void DisableHandChoosingUi(GameObject currentPreGameUi, CardDisplay[] cardDisplays)
    {
        mainCanvas.enabled = true;
        preGameButton.gameObject.SetActive(false);

        foreach (var cardDisplay in cardDisplays)
        {
            cardDisplay.frame.enabled = false;
        }

        currentPreGameUi.SetActive(false);
    }

    private void AddToHandOnPress(CardDisplay cardDisplay, CardDisplay[] allCardDisplays)
    {
        if (cardDisplay.frame.enabled)
        {
            GameState.Instance.currentPlayer.myTempHand.Remove(cardDisplay.myCard);
        }
        else
        {
            if (GameState.Instance.currentPlayer.myTempHand.Count >= 3)
            {
                return;
            }
 
            GameState.Instance.currentPlayer.myTempHand.Add(cardDisplay.myCard);
        }

        cardDisplay.frame.enabled = !cardDisplay.frame.enabled;
        CheckPreGameButton(allCardDisplays);
    }

    private void RemoveFromHandOnPress(CardDisplay cardDisplay, CardDisplay[] allCardDisplays)
    {
        if (cardDisplay.frame.enabled)
        {
            GameState.Instance.currentPlayer.myTempHand.Add(cardDisplay.myCard);
        }
        else
        {
            for (var i = 0; i < _handSize; i++)
            {
                if (allCardDisplays[i].frame.enabled == true)
                {
                    return;
                }
            }

            if (GameState.Instance.currentPlayer.myTempHand.Count < 3)
            {
                return;
            }

            GameState.Instance.currentPlayer.myTempHand.Remove(cardDisplay.myCard);
        }

        cardDisplay.frame.enabled = !cardDisplay.frame.enabled;
        CheckPreGameButton(allCardDisplays);
    }

    private void CheckPreGameButton(CardDisplay[] allCardDisplays)
    {
        bool isAnyCardChosen = false;
        foreach (var cardDisplay in allCardDisplays)
        {
            if (cardDisplay.frame.enabled == true)
            {
                isAnyCardChosen = true;
                break;      
            }
        }
        
        if (GameState.Instance.currentPlayer.myTempHand.Count == 3 && isAnyCardChosen)
        { 
            UiManager.Instance.preGameButton.gameObject.GetComponent<Image>().color = Color.white;
            UiManager.Instance.preGameButton.enabled = true;       
        }
        else
        {
            Color tempColor = new Color(255, 255, 255, 0.2f);
            UiManager.Instance.preGameButton.gameObject.GetComponent<Image>().color = tempColor;
            UiManager.Instance.preGameButton.enabled = false;
        }
    }

    public void EnableCardsUi()
    {
        foreach (var cardDisplay in mainCardDisplays)
        {
            cardDisplay.gameObject.SetActive(true);
            cardDisplay.SetCardUi(GameState.Instance.currentPlayer.myHand[System.Array.IndexOf(mainCardDisplays, cardDisplay)]);
            cardDisplay.ChangeCooldownUI();
        }
    }

    public void DisableCardsUi()
    {
        foreach (var cardDisplay in mainCardDisplays)
        {
            cardDisplay.gameObject.SetActive(false);
        }
    }

    public void DisableSupportingCards()
    {
        foreach (var cardDisplay in mainCardDisplays) 
        {
            if (!cardDisplay.myCard.CardData.isMain)
            {
                cardDisplay.darkFg.enabled = true;
            }
        }
    }
    
    public void ChangeBannerAndButtons(bool isStageStarting)
    {
        endTurnButton.SetActive(isStageStarting);
        startTurnButton.SetActive(!isStageStarting);

        if (isStageStarting)
        {
            switch (GameState.Instance.activeTeam)
            {
                case Team.RedTeam:

                    playerBanner.SetText("Ход красного!");
                    break;

                case Team.GreenTeam:

                    playerBanner.SetText("Ход зеленого!");
                    break;

                case Team.Null:

                    playerBanner.SetText("Фаза сражения!");
                    break;
            }
        }
        else
        {
            switch (GameState.Instance.activeTeam)
            {
                case Team.Null:

                    playerBanner.SetText("Красный, ты готов?");
                    break;

                case Team.RedTeam:

                    playerBanner.SetText("Зеленый, ты готов?");
                    break;
                case Team.GreenTeam:

                    playerBanner.SetText("Начать сражение?!");
                    break;

            }
        }
    }
}
