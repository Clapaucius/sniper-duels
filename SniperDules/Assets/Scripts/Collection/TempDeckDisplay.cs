﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TempDeckDisplay : DeckDisplay
{
    public Button readyButton;

    public override void Start()
    {
        base.Start();

        for (int i = 0; i < maxNumberOfCards; i++)
        {
                int x = i;
                myButtons[i].onClick.AddListener(delegate { ClearCardSlot(x); });
        }
    }

    public override void AddCardToSlot(Card card)
    {
        if (card == null) { return; }

        for (int i = 0; i < maxNumberOfCards; i++)
        {
            if (myDeck.myCards[i] == null)
            {
                myDeck.myCards[i] = card;
                cardNames[i].text = card.CardData.cardName;

                RefreshReadyButton();
                return;
            }
        }
    }

    public void GoToDeckList() //move method to CollectionManager!
    {
        CollectionManager.Instance.editingDeckDisplay.ClearAllMyCardSlots();
        SetDeck(CollectionManager.Instance.editingDeckDisplay, myDeck);
        CollectionManager.Instance.editingDeckDisplay.OnSettingDeck();

        if (CollectionManager.PlayerInCollection == PlayerInCollection.RedPlayer)
        {
            int indexOfEditingDeck = System.Array.IndexOf(UiCollectionManager.Instance.deckDisplays, CollectionManager.Instance.editingDeckDisplay);
            for (int i = 0; i < myDeck.myCards.Length; i++)
            {
                CollectionManager.Instance.redPlayerDecks[indexOfEditingDeck].myCards[i] = myDeck.myCards[i];
            }
        }
        else if (CollectionManager.PlayerInCollection == PlayerInCollection.GreenPlayer)
        {
            int indexOfEditingDeck = System.Array.IndexOf(UiCollectionManager.Instance.deckDisplays, CollectionManager.Instance.editingDeckDisplay);
            for (int i = 0; i < myDeck.myCards.Length; i++)
            {
                CollectionManager.Instance.greenPlayerDecks[indexOfEditingDeck].myCards[i] = myDeck.myCards[i];
            }
        }
        ClearAllMyCardSlots();
        UiCollectionManager.Instance.deckChoosingCanvas.enabled = true;
        UiCollectionManager.Instance.deckEditingCanvas.enabled = false;
    }

    public override void RefreshReadyButton()
    {
        if (IsDeckFull())
        {
            readyButton.enabled = true;
            readyButton.gameObject.GetComponent<Image>().color = Color.white;
        }
        else
        {
            readyButton.enabled = false;
            Color tempColor = new Color(255, 255, 255, 0.2f);
            readyButton.gameObject.GetComponent<Image>().color = tempColor;
        }
    }

    public override void SetDeck(DeckDisplay deckDisplay, Deck deck)
    {
        base.SetDeck(deckDisplay, deck);
    }

    public override void ClearCardSlot(int i)
    {
        base.ClearCardSlot(i);
        RefreshReadyButton();
    }
}
