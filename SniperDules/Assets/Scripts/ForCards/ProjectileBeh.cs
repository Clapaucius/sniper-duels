﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBeh : MonoBehaviour
{
    [HideInInspector] public Unit myUnit;
    [HideInInspector] public int damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject != myUnit.gameObject)
        {
            if (collision.GetComponent<Unit>() != null)
            {
                collision.GetComponent<Unit>().TakeDamage(damage);
            }
            Destroy(gameObject);
        }
    }

    protected virtual void OnDestroy()
    {
        
    }
}
