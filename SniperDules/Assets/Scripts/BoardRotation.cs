﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardRotation : MonoBehaviour
{

    public float rotationSpeed;

   

    public void Rotate()
    {
        StartCoroutine(Rotation());
    }

    IEnumerator Rotation()
    {
        while (transform.rotation != Quaternion.Euler(0, 0, 180))
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
            yield return null;
        }
    }
}
