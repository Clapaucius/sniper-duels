﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Deck
{
    public Card[] myCards = new Card[5];

    public Deck(Card[] cards)
    {
        myCards = cards;
    }

    public Deck() { }
}
