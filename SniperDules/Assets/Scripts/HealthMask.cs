﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthMask : MonoBehaviour
{
    public GameObject[] healthMasks;


    public void EnableMask(int missingHealth)
    {
        foreach (var mask in healthMasks)
        {
            mask.SetActive(false);
        }

        for (int i = 0; i < missingHealth; i++)
        {
            healthMasks[i].SetActive(true);
        }
    }

    
}
