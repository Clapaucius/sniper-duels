﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : WalkingUnit
{
    public Deck myDeck;
    public List<Card> myTempHand = new List<Card>();
    public List<Card> myHand = new List<Card>();
    public List<Card> myReserve = new List<Card>();
    public Card[] cardsNotInHand;
    
    public bool isSupportingCardPlayed;
    public bool isThisTurnFirstTurn;

    public Card playedSupportingCard;
    public Card playedMainCard;

    private void Awake()
    {
        isThisTurnFirstTurn = true;
        if (team == Team.RedTeam && CollectionManager.MainRedPlayerDeck != null)
        {
            myDeck = CollectionManager.MainRedPlayerDeck;
        }
        else if (team == Team.GreenTeam && CollectionManager.MainGreenPlayerDeck != null)
        {
            myDeck = CollectionManager.MainGreenPlayerDeck;
        }
        else
        {
            Deck testDeck = new Deck(GameState.Instance.testCards);
            myDeck = testDeck;
        }

        if (GameState.Instance.IsTesting)
        {
            isThisTurnFirstTurn = false;
            foreach (var card in GameState.Instance.testCards)
            {
                myHand.Add(Instantiate(card));
            }
        }
    }

    public override void Start()
    {
        base.Start();
        //   SetTestHand();
      //  isThisTurnFirstTurn = true;

    }

    protected override IEnumerator StartAction(Vector2 tilePos)
    {
        if (playedSupportingCard != null)
        {
            playedSupportingCard.Play();
        }

        if (true)
        {

        yield return StartCoroutine(base.StartAction(tilePos));
        }

        if (playedMainCard != null)
        {
            playedMainCard.Play();
        }

        ResetPlayedCards();

    }

    public override void OnDeath()
    {
        //base.OnDeath();   // пусть останется прозрачная подложка
        if (team == Team.GreenTeam)
        {
            GameState.NumberOfGreenPlayerDeaths++;
            UiManager.Instance.EnableDeathScreen(GameState.Instance.redPlayer);
        }
        else
        {
            GameState.NumberOfRedPlayerDeaths++;
            UiManager.Instance.EnableDeathScreen(GameState.Instance.greenPlayer);
        }

        
       

    }

    private void ResetPlayedCards()
    {       
        playedMainCard = null;
        playedSupportingCard = null;
        isSupportingCardPlayed = false;
    }
}

