﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class MainDeckDisplay : DeckDisplay
{
    public Button mainButton;
    
    public override void Start()
    {
        if (!IsDeckFull())
        {
            Color tempColor = new Color(255, 255, 255, 0.2f);
            mainButton.gameObject.GetComponent<Image>().color = tempColor;
            mainButton.enabled = false;
        }

        if (AreTheseDecksEqual(myDeck, CollectionManager.MainGreenPlayerDeck) &&
            CollectionManager.PlayerInCollection == PlayerInCollection.GreenPlayer)
        {
            UpdateAllDeckDisplaysMainButtons();
        }
        else if (AreTheseDecksEqual(myDeck, CollectionManager.MainRedPlayerDeck) &&
                 CollectionManager.PlayerInCollection == PlayerInCollection.RedPlayer)
        { 
            UpdateAllDeckDisplaysMainButtons();
        }
    }

    public void GoToEditor()
    {
        CollectionManager.Instance.editingDeckDisplay = this;

        UiCollectionManager.Instance.deckChoosingCanvas.enabled = false;
        UiCollectionManager.Instance.deckEditingCanvas.enabled = true;

        SetDeck(CollectionManager.Instance.tempDeckDisplay, myDeck);

        UiCollectionManager.Instance.CheckAllCardDisplays();

        CollectionManager.Instance.tempDeckDisplay.RefreshReadyButton();
    }

    public override void OnSettingDeck()
    {
        RefreshMainButton();
        MakeTheDeckMainDeck();
    }
    
    private void UpdateAllDeckDisplaysMainButtons()
    {
        foreach (var deckDisplay in UiCollectionManager.Instance.deckDisplays)
        {
            deckDisplay.RefreshMainButton();
        }

        mainButton.enabled = false;
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Основная";
    }

    public void MakeTheDeckMainDeck()
    {
        if (CollectionManager.PlayerInCollection == PlayerInCollection.GreenPlayer)
        {
            CollectionManager.MainGreenPlayerDeck = myDeck;
        }
        else
        {
            CollectionManager.MainRedPlayerDeck = myDeck;
        }

        UpdateAllDeckDisplaysMainButtons();
    }

    public override void RefreshMainButton()
    {
        if (IsDeckFull())
        {
            mainButton.gameObject.GetComponent<Image>().color = Color.white;

            mainButton.enabled = true;
            mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Сделать основной";

        }
    }

    private bool AreTheseDecksEqual(Deck deck1, Deck deck2)
    {
        if (deck1 == null || deck2 == null)
        {
            return false;
        }
        
        for (int i = 0; i < deck1.myCards.Length; i++)
        {
            if (deck1.myCards[i] != deck2.myCards[i])
            {
                return false;
            }
        }
        return true;
    }
}
