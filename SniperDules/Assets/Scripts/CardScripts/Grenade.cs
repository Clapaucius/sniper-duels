﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Card
{
    [SerializeField] private float throwRange = 9f;
    public override void Play()
    {
       GameObject grenade = Instantiate(CardData.cardAsset, ChosenTile.transform.position, Quaternion.identity);
       grenade.GetComponent<GrenadeBeh>().damage = CardData.damage;
    }

    public override string GetDescription()
    {
        return string.Format(description, CardData.damage.ToString());
    }

    protected override void GetCreatableTiles()
    {
        float yDistance;
        foreach (var tile in TileManager.Instance.allTiles)
        {
            yDistance = Mathf.Abs(tile.transform.position.y - CastingPoint.y);
            
            bool isTargetNotFarFromCaster = yDistance <= throwRange;
            bool isCasterThere = (Vector2)tile.transform.position != CastingPoint;
            if (isCasterThere && isTargetNotFarFromCaster)
            {
                tile.SetCreatable();
            }
        }
    }
}
