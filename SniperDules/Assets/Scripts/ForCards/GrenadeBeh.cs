﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeBeh : MonoBehaviour
{
    [HideInInspector] public Player myPlayer;
    [HideInInspector] public int damage;

    [SerializeField] private LayerMask _obstacleLayer;
    
    private void Start()
    {
        GameState.Instance.BeginningOfTurnAction += TryToDetonate;
    }

    private void TryToDetonate()
    {
        if (GameState.Instance.activeTeam != Team.Null) return;
        GameState.Instance.BeginningOfTurnAction -= TryToDetonate;
        StartCoroutine(Detonate());
    }
    
    private IEnumerator Detonate()
    {
        yield return new WaitForSeconds(0.6f);

        Collider2D[] obstacles = Physics2D.OverlapCircleAll(transform.position, 1.5F, _obstacleLayer);

        foreach (var obstacle in obstacles)
        {
            obstacle.GetComponent<Unit>().TakeDamage(damage);
        }

        yield return new WaitForSeconds(0.2f);
        
        Destroy(gameObject);
    }
}
