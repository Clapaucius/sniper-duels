﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayCard : MonoBehaviour
{

    private CardDisplay _selectedCardDisplay;
    public bool IsPlayerChoosingTile { get; private set; }
    
    public void CheckCard(CardDisplay currentCardDisplay)
    {
        bool canUseMainCard = currentCardDisplay.myCard.CardData.isMain &&
                              GameState.Instance.currentPlayer.playedMainCard == null &&
                              currentCardDisplay.myCard.CurrentCooldown == 0;
        bool canUseSupportingCard = !currentCardDisplay.myCard.CardData.isMain &&
                                    !GameState.Instance.currentPlayer.isSupportingCardPlayed &&
                                    currentCardDisplay.myCard.CurrentCooldown == 0;

        if (canUseMainCard || canUseSupportingCard)
        {
            GameState.Instance.ResetTilesAndUnits();
            currentCardDisplay.ChangeFrame();
            if (_selectedCardDisplay == currentCardDisplay)
            {
                _selectedCardDisplay = null;
                IsPlayerChoosingTile = false;
            }
            else
            {
                if (_selectedCardDisplay)
                {
                    _selectedCardDisplay.ChangeFrame();
                }
                _selectedCardDisplay = currentCardDisplay;
                currentCardDisplay.myCard.GetCastingPlayer(GameState.Instance.currentPlayer);
                currentCardDisplay.myCard.BeginChoosingTarget();
                IsPlayerChoosingTile = true;
            }
        }
    }

    public void PrepareCardToPlay(Card card)
    {
        StartCoroutine(EnableAccessWithDelay());
        _selectedCardDisplay.ChangeFrame();
        _selectedCardDisplay = null;
        card.SetCardCooldown();
        
        if (card.CardData.isMain)
        {
            GameState.Instance.currentPlayer.playedMainCard = card;

            foreach (var cardDisplay in UiManager.Instance.mainCardDisplays)
            {
                if (cardDisplay.myCard.CardData.isMain)
                {
                    cardDisplay.ChangeForeground();
                }
            }
        }
        else
        {
            GameState.Instance.currentPlayer.isSupportingCardPlayed = true;
            GameState.Instance.currentPlayer.playedSupportingCard = card;

            foreach (var cardDisplay in UiManager.Instance.mainCardDisplays)
            {
                if (!cardDisplay.myCard.CardData.isMain)
                {
                    cardDisplay.ChangeForeground();
                }
            }
        }
    }

    private IEnumerator EnableAccessWithDelay()
    {
        yield return new WaitForSeconds(0.01f);
        IsPlayerChoosingTile = false;
    }
}
