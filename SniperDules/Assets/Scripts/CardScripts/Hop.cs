﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hop : Card
{
    public override void BeginChoosingTarget()
    {
       // GameMaster.Instance.selectedUnit = CastingPlayer;
        base.BeginChoosingTarget();
    }

    public override void GetChosenTile(Tile chosenTile)
    {
        base.GetChosenTile(chosenTile);
        TileManager.Instance.CreateFuturePositionMark(ChosenTile, CastingPlayer.gameObject);
    }

    public override string GetDescription()
    {
        return string.Format(description);
    }

    protected override void GetCreatableTiles()
    {
        float xDistance, yDistance;
        foreach (var tile in TileManager.Instance.allTiles)
        {
            xDistance = Mathf.Abs(tile.transform.position.x - CastingPlayer.transform.position.x);
            yDistance = Mathf.Abs(tile.transform.position.y - CastingPlayer.transform.position.y);
            if ((yDistance == 0) && (xDistance == 2) && tile.IsClear())
            {
                tile.SetCreatable();
            }
        }
    }
}
