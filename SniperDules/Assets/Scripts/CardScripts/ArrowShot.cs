﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowShot : Card  
{
     public float arrowSpeed;

     public override void Play()
    {
        int directionModifier = CastingPlayer.team == Team.GreenTeam ? -1 : 1;

        GameObject arrowInst = Instantiate(CardData.cardAsset, CastingPoint, Quaternion.identity);
        arrowInst.GetComponent<Rigidbody2D>().velocity = Vector2.up * arrowSpeed * directionModifier;
        arrowInst.GetComponent<ProjectileBeh>().myUnit = CastingPlayer;
        arrowInst.GetComponent<ProjectileBeh>().damage = CardData.damage;
    }

    public override string GetDescription()
    {
        return string.Format(description, CardData.damage.ToString());
    }

    protected override void GetCreatableTiles()
    {
        foreach (var tile in TileManager.Instance.allTiles)
        {
            bool doesTileHaveThaSameX = tile.transform.position.x == CastingPoint.x;
            if (doesTileHaveThaSameX)
            {
                bool isTileInFrontOfCaster;
                if (CastingPlayer.team == Team.GreenTeam)
                {
                    isTileInFrontOfCaster = tile.transform.position.y < CastingPoint.y;
                }
                else
                {
                    isTileInFrontOfCaster = tile.transform.position.y > CastingPoint.y;
                }

                if (isTileInFrontOfCaster)
                {
                    tile.SetCreatable();
                }
            }
        }
    }

    
}
