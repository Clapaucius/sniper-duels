﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DeckDisplay : MonoBehaviour
{
    public Deck myDeck = new Deck();
    public Button[] myButtons;

    public TextMeshProUGUI[] cardNames;

    public int maxNumberOfCards;

    public void Awake()
    {
        maxNumberOfCards = myDeck.myCards.Length;
        cardNames = new TextMeshProUGUI[maxNumberOfCards];
        for (int i = 0; i < maxNumberOfCards; i++)
        {
            cardNames[i] = myButtons[i].gameObject.GetComponentInChildren<TextMeshProUGUI>();
        }
    }
    public virtual void Start()
    {
    }

    public virtual void AddCardToSlot(Card card)
    {
        if (card == null) { return; }
        
        for (int i = 0; i < maxNumberOfCards; i++)
        {
            if (myDeck.myCards[i] == null)
            {
                myDeck.myCards[i] = card;
                cardNames[i].text = card.CardData.cardName;
                return;
            }
        }
    }

    public virtual void SetDeck(DeckDisplay deckDisplay, Deck deck)
    {
        foreach (Card card in deck.myCards)
        {
            deckDisplay.AddCardToSlot(card);
        }
    }

    public void ClearAllMyCardSlots()
    {
        for (int i = 0; i < maxNumberOfCards; i++)
        {
            ClearCardSlot(i);
        }
    }

    public virtual void  ClearCardSlot(int i)
    {
        if (myDeck.myCards[i] != null)
        {
            myDeck.myCards[i] = null;
            cardNames[i].text = "";

            UiCollectionManager.Instance.CheckAllCardDisplays();
        }
    }
    
    public bool IsDeckFull()
    {
        foreach (var card in myDeck.myCards)
        {
            if (card == null)
            {
                return false;
            }
        }
        return true;
    }

    public virtual void RefreshReadyButton()
    {

    }

    public virtual void RefreshMainButton()
    {

    }

    public virtual void OnSettingDeck()
    {

    }

}
