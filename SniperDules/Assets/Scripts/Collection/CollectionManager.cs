﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;


    public enum PlayerInCollection { RedPlayer, GreenPlayer };

public class CollectionManager : MonoBehaviour
{
    public static PlayerInCollection PlayerInCollection;// = PlayerInCollection.greenPlayer;

    public Deck[] greenPlayerDecks = new Deck[4];
    public Deck[] redPlayerDecks = new Deck[4];

    public static Deck MainGreenPlayerDeck;
    public static Deck MainRedPlayerDeck;

    public DeckDisplay editingDeckDisplay;
    public DeckDisplay tempDeckDisplay;

    public static CollectionManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;    
    }

    private void Start()
    {
        Load();

        if (PlayerInCollection == PlayerInCollection.RedPlayer)
        {
            UiCollectionManager.Instance.SetDecks(redPlayerDecks);
        }
        else
        {
            UiCollectionManager.Instance.SetDecks(greenPlayerDecks);
        }

        UiCollectionManager.Instance.SetBanner(PlayerInCollection);
    }

    public void AddCardToDeck(CardDisplay cardDisplay)
    {
        if (!cardDisplay.isLocked)
        {
            tempDeckDisplay.AddCardToSlot(cardDisplay.myCard);
            UiCollectionManager.Instance.CheckAllCardDisplays();
        }
    }

    public void ReturnToMenu()
    {
        Save();
        SceneManager.LoadScene(1);
    }

    public bool IsThisCardInDeckAlready(Card card)
    {
        if (Array.Exists(tempDeckDisplay.myDeck.myCards, x => x == card))
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private void Save()
    {
        Save saveData = new Save
        {
            greenPlayerDecks = greenPlayerDecks,
            redPlayerDecks = redPlayerDecks,
            mainGreenPlayerDeck = MainGreenPlayerDeck,
            mainRedPlayerDeck = MainRedPlayerDeck
        };

//Debug.Log(saveData.mainRedPlayerDeck.myCards[1]);
        string jsonData = JsonUtility.ToJson(saveData);
        PlayerPrefs.SetString("OurDecks", jsonData);
        PlayerPrefs.Save();
    }

    private void Load()
    {
        string jsonData = PlayerPrefs.GetString("OurDecks");
        Save loadedData = JsonUtility.FromJson<Save>(jsonData);

        if (loadedData == null)
        {
            return;
        }
        
        greenPlayerDecks = loadedData.greenPlayerDecks;
        redPlayerDecks = loadedData.redPlayerDecks;
    }
}
