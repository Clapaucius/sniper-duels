﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : Card
{
    
    public override void Play()
    {
        Instantiate(CardData.cardAsset, ChosenTile.transform.position, Quaternion.identity);
    }
    
    public override string GetDescription()
    {
        return string.Format(description, CardData.cardAsset.GetComponent<Unit>().MAXHealth.ToString());
    }


    protected override void GetCreatableTiles()
    {
        float xDistance, yDistance;
        foreach (var tile in TileManager.Instance.allTiles)
        {
            xDistance = Mathf.Abs(tile.transform.position.x - CastingPlayer.transform.position.x);
            yDistance = Mathf.Abs(tile.transform.position.y - CastingPlayer.transform.position.y);
            if (xDistance <= 1 && yDistance <= 1 && tile.IsClear())
            {
                tile.SetCreatable();
            }
        }
    }

}
