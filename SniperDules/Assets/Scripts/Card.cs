﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Card : MonoBehaviour
{
    [SerializeField] private CardData data;
    [TextArea(5, 10)] [SerializeField] protected string description;

    protected Tile ChosenTile;
    protected Unit ChosenUnit; // Do I need this?!
    protected Player CastingPlayer;        
    protected Vector2 CastingPoint;
    
    public CardData CardData => data;
    public int CurrentCooldown { get; private set; }

    public virtual void Play()
    {
    }

    private Vector2 GetChronologicalPlayerPosition()
    {
        Vector2 chronologicalPlayerPosition;
        if (GameState.Instance.currentPlayer.hasMoved)
        {
            // Future Player's position
            chronologicalPlayerPosition = CastingPlayer.futurePosition;  
        }
        else
        {
            // Present Player's position
            chronologicalPlayerPosition = CastingPlayer.transform.position;       
        }
        return chronologicalPlayerPosition;
    }
    public virtual void BeginChoosingTarget()
    {
        CastingPoint = GetChronologicalPlayerPosition();
        GetCreatableTiles();
        TileManager.Instance.SetCardJustPlayed(this);
    }

    #region GettingChoosedObjects
    public void GetCastingPlayer(Player castingPlayer)
    {
        CastingPlayer = castingPlayer;
    }
    public virtual void GetChosenTile(Tile chosenTile)
    {
        ChosenTile = chosenTile;
        if (CardData.DoesThisCardSpawnUnit)
        {
            TileManager.Instance.CreateFuturePositionMark(ChosenTile, data.cardAsset);
        }

        if (CardData.DoesThisCardRefreshPlayerMovement)
        {
            RefreshMovement();
        }
    }
    public void GetChosenUnit(Unit chosenUnit)// Do I need this? Its better to highlight tile instead.
    {
        ChosenUnit = chosenUnit;
    }
    
    public void SetCardCooldown()
    { 
        CurrentCooldown = CardData.cooldown + 1;
    }
    #endregion
        
    public void ReduceCurrentCooldown()
    {
        if (CurrentCooldown != 0)
        {
            CurrentCooldown--;
        }
    }

    protected virtual void GetCreatableTiles()
    {
    }

    public virtual string GetDescription()
    {
        return string.Format(description);
    }
    
    protected virtual void RefreshMovement()
    {     
        GameState.Instance.currentPlayer.hasMoved = false;
        GameState.Instance.currentPlayer.GetWalkableTiles();
    }
    
    
}
