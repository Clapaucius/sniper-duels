﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Save
{
    public Deck[] greenPlayerDecks = new Deck[4];
    public Deck[] redPlayerDecks = new Deck[4];

    public Deck mainGreenPlayerDeck;
    public Deck mainRedPlayerDeck;
}
