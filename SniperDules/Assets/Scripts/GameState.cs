﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Round { Round1, Round2, Round3 }


public class GameState : MonoBehaviour
{
    public static GameState Instance
    {
        get;
        private set;
    }

    public bool IsTesting;

    private static List<Card> _greenPlayerSaveHand;
    private static List<Card> _redPlayerSaveHand;
    private static List<Card> _greenPlayerSaveReserve;
    private static List<Card> _redPlayerSaveReserve;

    public static int NumberOfGreenPlayerDeaths;
    public static int NumberOfRedPlayerDeaths;
    private static Round _round;

    public Team activeTeam;

    public Player redPlayer;
    public Player greenPlayer;
    public Player currentPlayer;

    public event Action BeginningOfTurnAction;
    public event Action EndOfTurnAction;

    public WalkingUnit selectedUnit;

    public Card[] testCards;

    private delegate void FirstTurnDelegate();

    private FirstTurnDelegate _firstTurnDelegate = delegate { };

    private CardDisplay[] _firstTurnCardDisplays;
    private GameObject _currentPreGameUi;

    private void Awake()
    {
        Instance = this;

        if (_round == Round.Round1) return;
        redPlayer.myTempHand = _redPlayerSaveHand;
        redPlayer.myReserve = _redPlayerSaveReserve;

        greenPlayer.myTempHand = _greenPlayerSaveHand;
        greenPlayer.myReserve = _greenPlayerSaveReserve;

    }

    private void Start()
    {
        currentPlayer = redPlayer;

        activeTeam = Team.Null;
        BeginRound();
    }

    private void BeginRound()
    {
        switch (_round)
        {
            case Round.Round1:

                redPlayer.cardsNotInHand = redPlayer.myDeck.myCards;
                greenPlayer.cardsNotInHand = greenPlayer.myDeck.myCards;

                _firstTurnCardDisplays = UiManager.Instance.firstPreGameCardDisplays;
                _currentPreGameUi = UiManager.Instance.preGameUiArray[0];
                _firstTurnDelegate = FirstRoundButtonEvent;
                _round = Round.Round2;
                break;

            case Round.Round2:

                redPlayer.cardsNotInHand = _redPlayerSaveHand.Concat(_redPlayerSaveReserve).ToArray();
                greenPlayer.cardsNotInHand = _greenPlayerSaveHand.Concat(_greenPlayerSaveReserve).ToArray();

                _firstTurnCardDisplays = UiManager.Instance.secondPreGameCardDisplays;
                _currentPreGameUi = UiManager.Instance.preGameUiArray[1];
                _firstTurnDelegate = SecondRoundButtonEvent;
                _round = Round.Round3; 
                break;

            case Round.Round3:

                redPlayer.cardsNotInHand = _redPlayerSaveHand.Concat(_redPlayerSaveReserve).ToArray();
                greenPlayer.cardsNotInHand = _greenPlayerSaveHand.Concat(_greenPlayerSaveReserve).ToArray();
                
                _firstTurnCardDisplays = UiManager.Instance.thirdPGameCardDisplays;

                _currentPreGameUi = UiManager.Instance.preGameUiArray[2];
                break;
        }

        UiManager.Instance.SetScore();
        UiManager.Instance.EnableCardChoosingUi(_firstTurnCardDisplays);
    }

    public void EndHandChoosing()
    {
        UiManager.Instance.DisableHandChoosingUi(_currentPreGameUi, _firstTurnCardDisplays);
        _firstTurnDelegate(); 

        if (currentPlayer.team == Team.RedTeam)
        {
            _redPlayerSaveHand = currentPlayer.myTempHand;
            _redPlayerSaveReserve = currentPlayer.myReserve;
        }
        else
        {
            _greenPlayerSaveHand = currentPlayer.myTempHand;
            _greenPlayerSaveReserve = currentPlayer.myReserve;
        }

        foreach (var card in currentPlayer.myTempHand)
        {
            currentPlayer.myHand.Add(Instantiate(card));
        }
    }

    private void FirstRoundButtonEvent()
    {
        foreach (var card in currentPlayer.myDeck.myCards)
        {
            if (currentPlayer.myTempHand.Contains(card) == false)
            {
                currentPlayer.myReserve.Add(card);
            }
        }
    }

    private void SecondRoundButtonEvent()
    {
        foreach (var card in currentPlayer.myDeck.myCards)
        {
            if (currentPlayer.myTempHand.Contains(card) && currentPlayer.myReserve.Contains(card))
            {
                currentPlayer.myReserve.Remove(card);
            }
        }
    }

    public void PlayerTurn()
    {
        if (currentPlayer.isThisTurnFirstTurn)
        {
            UiManager.Instance.SetCardsToChoose(_currentPreGameUi, _firstTurnCardDisplays, currentPlayer.cardsNotInHand);
            currentPlayer.isThisTurnFirstTurn = false;
            return;
        }

        ReduceAllCooldowns();
        UiManager.Instance.EnableCardsUi();

        foreach (var walkingUnit in FindObjectsOfType<WalkingUnit>())
        {
            if (walkingUnit.team == activeTeam)
            {
                walkingUnit.hasMoved = false;
            }
        }   
    }

    public void EndCurrentStage()
    {
        ResetTilesAndUnits();
        TileManager.Instance.DestroyAllMarks();

        UiManager.Instance.DisableCardsUi();
        UiManager.Instance.ChangeBannerAndButtons(false);

        EndOfTurnAction?.Invoke();
    }

    private void ReduceAllCooldowns()
    {
        foreach (var card in currentPlayer.myHand)
        {
            card.ReduceCurrentCooldown();
        }
    }

    public void BeginCurrentStage()
    {
        switch (activeTeam)
        {
            case Team.Null:

                currentPlayer = redPlayer;
                activeTeam = Team.RedTeam;
                PlayerTurn();
                break;

            case Team.RedTeam:

                currentPlayer = greenPlayer;
                activeTeam = Team.GreenTeam;
                PlayerTurn();
                break;

            case Team.GreenTeam:

                currentPlayer = null;
                activeTeam = Team.Null;
                Action();
                break;
        }

        UiManager.Instance.ChangeBannerAndButtons(true);
        BeginningOfTurnAction?.Invoke();
    } 

    private void Action()
    {
        ResetTilesAndUnits();
        foreach (var walkingUint in FindObjectsOfType<WalkingUnit>())//array
        {
            walkingUint.UnitAction();
        }
    }

    public void ResetTilesAndUnits()
    {
        TileManager.Instance.ResetAllTiles();

        foreach (var unit in FindObjectsOfType<Unit>())//array
        {
            if (unit.isTargetable)
            {
                unit.Reset();
            }
        }
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(3);
    }

    public void ReturnToMenu()
    {
        _round = Round.Round1;
        SceneManager.LoadScene(1);
    }
}