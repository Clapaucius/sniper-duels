﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Starter : MonoBehaviour
{
    void Start()
    {
        Load();
        SceneManager.LoadScene(1); 
    }
    
    private void Load()
    {
        string jsonData = PlayerPrefs.GetString("OurDecks");
        Save loadedData = JsonUtility.FromJson<Save>(jsonData);

        if (loadedData == null)
        {
            return;
        }
        
        CollectionManager.MainGreenPlayerDeck = loadedData.mainGreenPlayerDeck;
        CollectionManager.MainRedPlayerDeck = loadedData.mainRedPlayerDeck;
        Debug.Log($"{CollectionManager.MainRedPlayerDeck}");
    }

}
