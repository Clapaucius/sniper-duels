﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeBeh : MonoBehaviour
{
    private SpriteRenderer _rend;
    public Player myPlayer;
    private Color _transparentColor;

    private void Start()
    {
        _rend = GetComponent<SpriteRenderer>();
        _transparentColor = _rend.color;
        _transparentColor.a = 0.4f;
        
        GameState.Instance.BeginningOfTurnAction += DisableSmoke;
        GameState.Instance.EndOfTurnAction += EnableSmoke;
    }

    private void EnableSmoke()
    {
        if (myPlayer == GameState.Instance.currentPlayer)
        {
            _rend.color = Color.white;
        }
    }
    
    private void DisableSmoke()
    {
        if (myPlayer == GameState.Instance.currentPlayer)
        {
            _rend.color = _transparentColor;
        }
        else if (GameState.Instance.activeTeam == Team.Null)
        {
            DestroySmoke();
        }
    }

    private void DestroySmoke()
    {
        GameState.Instance.EndOfTurnAction -= EnableSmoke;
        GameState.Instance.BeginningOfTurnAction -= DisableSmoke;
        Destroy(gameObject);
    }

    
}
