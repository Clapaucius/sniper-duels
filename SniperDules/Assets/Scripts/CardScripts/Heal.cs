﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Card
{
    [SerializeField] private int _healAmount;

    private void Start()
    {
        _healAmount = CardData.damage;
    }
    public override void Play()
    {
        ChosenUnit.Heal(_healAmount);
    }

    public override string GetDescription()
    {
        return string.Format(description, _healAmount.ToString());
    }

    protected override void GetCreatableTiles()
    {
        foreach (var unit in FindObjectsOfType<WalkingUnit>())
        {
            if (unit.team == CastingPlayer.team )
            {
                unit.SetTargetable(this);
            }
        }
    }

}
