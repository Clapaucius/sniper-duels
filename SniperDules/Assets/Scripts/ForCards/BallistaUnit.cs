﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallistaUnit : MonoBehaviour
{
    public GameObject arrow;
   // [HideInInspector]
    public Player castingPlayer;
    [HideInInspector] public int damage;
    
    [SerializeField] private float arrowSpeed;
    private int _directionModifier;

    private void Start()
    {
        _directionModifier = castingPlayer.team == Team.GreenTeam ? -1 : 1;
        GameState.Instance.BeginningOfTurnAction += Shoot;
    }

    private void Shoot()
    {
        if (GameState.Instance.activeTeam != Team.Null) return;
        
        GameObject arrowInst = Instantiate(arrow, transform.position, Quaternion.identity);
        arrowInst.GetComponent<Rigidbody2D>().velocity = Vector2.up * arrowSpeed * _directionModifier;
        arrowInst.GetComponent<ProjectileBeh>().myUnit = GetComponent<Unit>();
        arrowInst.GetComponent<ProjectileBeh>().damage = damage;
    }

    private void OnDestroy()
    {
        GameState.Instance.BeginningOfTurnAction -= Shoot;
    }
}
