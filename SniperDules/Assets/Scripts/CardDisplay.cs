﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Serialization;

public class CardDisplay : MonoBehaviour
{

    public Card myCard;


    public TextMeshProUGUI nameText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI typeText;
    public TextMeshProUGUI cooldownText;
    public TextMeshProUGUI currentCooldownText;
    [FormerlySerializedAs("darkFG")] public Image darkFg;
    public Image art;
    public Image frame;

    public bool isLocked = false;

    public void SetCardUi(Card card)
    {
        myCard = card;

        nameText.text = card.CardData.cardName;
        descriptionText.text = card.GetDescription();
        art.sprite = card.CardData.artwork;
        cooldownText.text = card.CardData.cooldown == 0 ? "" : card.CardData.cooldown.ToString();
        typeText.text = card.CardData.isMain ? "Основная" : "Вспомогательная";
    }

    public void ChangeCooldownUI()
    {
        if (myCard.CurrentCooldown > 0)
        {
            currentCooldownText.text = myCard.CurrentCooldown.ToString();
            darkFg.enabled = true;
        }
        else
        {
            darkFg.enabled = false;
            currentCooldownText.text = "";
        }
    }

    public void ChangeForeground()
    {
        darkFg.enabled = !darkFg.enabled;
    }
    public void ChangeFrame()
    {
        frame.enabled = !frame.enabled;
    }

    public void LockCardDisplay()
    {
        darkFg.enabled = true;
        isLocked = true;
    }

    public void UnlockCardDisplay()
    {
        darkFg.enabled = false;
        isLocked = false;
    }
}
