﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team { RedTeam, GreenTeam, Null}

public class Unit : MonoBehaviour
{
    public Team team;  
    public Color originalColor;
    public bool isTargetable;
    public Card cardJustPlayed; // Move to Unit Controller
    
    [SerializeField] private int maxHealth;
    [SerializeField] private SpriteRenderer rend;
    [SerializeField] private HealthMask healthMask;


    public int MAXHealth => maxHealth;
    public int Health { get; private set; }
    public virtual void Start()
    {
        originalColor = rend.color;
      //  rend = GetComponentInChildren<SpriteRenderer>();
        healthMask = GetComponentInChildren<HealthMask>();
        Health = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        if (damage > 0)
        {
            Health -= damage;
            healthMask.EnableMask(maxHealth - Health);
        }

        if (Health <= 0)
        {
            OnDeath();
        }
    }

    public virtual void OnDeath()
    {
        Destroy(gameObject, 0.3f);
    }

    public void Heal(int healAmount)
    {
        if (maxHealth - Health >= healAmount)
        {
            Health += healAmount;
            healthMask.EnableMask(maxHealth - Health);
        }
        else if (maxHealth - Health < healAmount )
        {
            Health = maxHealth;
            healthMask.EnableMask(maxHealth - Health);
        }
    }
    
    public void SetTargetable(Card card)
    {
        isTargetable = true;
        Color darkerColor = originalColor * 0.4f;
        darkerColor.a = 1f;
        rend.color = darkerColor;
        cardJustPlayed = card;
    }

    public void Reset()
    {
        rend.color = originalColor;
        isTargetable = false;
        cardJustPlayed = null;
    }
}
