﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Button startGameButton;

    private Color _buttonColor;
    private Image _myImage;

    private void Start()
    {
       
        _myImage = startGameButton.gameObject.GetComponent<Image>();

        if (startGameButton.enabled)
        {
            _buttonColor = _myImage.color;
        }
        
        if ((CollectionManager.MainGreenPlayerDeck != null) && (CollectionManager.MainRedPlayerDeck != null))
        {
            _myImage.color = _buttonColor;
            startGameButton.enabled = true;
        }
        else
        {
            Color tempColor = _myImage.color;
            tempColor.a = 0.2f;
            _myImage.color = tempColor;
            startGameButton.enabled = false;
        }
    }

    public void LoadGreenCollectionScene()
    {
        CollectionManager.PlayerInCollection = PlayerInCollection.GreenPlayer;
        SceneManager.LoadScene(2); 
    }

    public void LoadRedCollectionScene()
    {
        CollectionManager.PlayerInCollection = PlayerInCollection.RedPlayer;
        SceneManager.LoadScene(2); 
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene(3); 
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void EnableOptions()
    {
        PlayerPrefs.DeleteKey(("OurDecks"));
    }
    
    private void CreateFirstDecks()
    {
        CollectionManager.MainGreenPlayerDeck = new Deck();
        CollectionManager.MainRedPlayerDeck = new Deck();
    }

    [ContextMenu("Clear Prefs")]
    public void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
