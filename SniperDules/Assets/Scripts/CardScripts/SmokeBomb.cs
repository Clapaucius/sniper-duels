﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeBomb : Card
{
    public override void Play()
    {
        GameObject smoke = Instantiate(CardData.cardAsset, ChosenTile.transform.position, Quaternion.identity);
        smoke.GetComponent<SmokeBeh>().myPlayer = CastingPlayer;
    }

    public override string GetDescription()
    {
        return string.Format(description);
    }

    protected override void GetCreatableTiles()
    {
        foreach (var tile in TileManager.Instance.allTiles)
        {
            if ((Vector2)tile.transform.position == (Vector2)CastingPlayer.transform.position)
            {
                tile.SetCreatable();
            }
        }
    }
}
