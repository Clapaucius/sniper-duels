﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteorite : Card
{
    [SerializeField] private Color _groundOnFire;
    private LayerMask _obstacleLayer;
    
    public override void Play()
    {
        _obstacleLayer = TileManager.Instance.obstacleLayer;
        StartCoroutine(PlayMeteorite());
    }

    public override void BeginChoosingTarget()
    {
        base.BeginChoosingTarget();
    }

    public override string GetDescription()
    {
        return string.Format(description, CardData.damage.ToString());
    }

    private IEnumerator PlayMeteorite()
    {
        yield return new WaitForSeconds(0.6f);

        Collider2D obstacle = Physics2D.OverlapCircle(ChosenTile.transform.position, 0.2F, _obstacleLayer);
        if (obstacle != null)
        {
            obstacle.GetComponent<Unit>().TakeDamage(CardData.damage);
        }
        ChosenTile.Highlight(_groundOnFire);

        yield return new WaitForSeconds(2f);

        ChosenTile.Highlight(Color.white);
    }

    protected override void GetCreatableTiles()
    {      
        foreach (var tile in TileManager.Instance.allTiles)
        {
            if ((Vector2)tile.transform.position != CastingPoint)
            {
                tile.SetCreatable();
            }
        }
    }
}
