﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "New CardData", menuName = "CardData")]
public class CardData : ScriptableObject
{
    public string cardName;
    public Sprite artwork;
    public int damage;
    public int cooldown;
    public GameObject cardAsset;
    public bool isMain;
    public bool DoesThisCardRefreshPlayerMovement;
    public bool DoesThisCardSpawnUnit;

}
