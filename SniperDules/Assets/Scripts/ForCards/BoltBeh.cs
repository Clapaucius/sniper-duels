﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltBeh : ProjectileBeh
{
    public float boltSpeed;
    public Vector2 chosenTilePosition;
    [Tooltip("Pause between charges in seconds")] public float pause = 0.1f;
    private Vector2 _targetPosition;
    private int _xDirectionMultiplier = 1;
    // Left or right.
    private int _directionModifier;
    // Up or down.
    private int _playerModifier;

    public void StartMovement()
    {
        _playerModifier = myUnit.team == Team.RedTeam ? 1 : -1;
        _directionModifier = chosenTilePosition.x > transform.position.x ? 1 : -1;
        
        _targetPosition = new Vector2(1, 1);
        StartCoroutine(SegmentOfMovement());
    }

    private IEnumerator SegmentOfMovement()
    {
        Vector2 modifiedTargetPosition = _targetPosition;
        modifiedTargetPosition.x *= _directionModifier * _playerModifier;
        modifiedTargetPosition *= _playerModifier;
        
        Vector2 goal = (Vector2) myUnit.transform.position + modifiedTargetPosition;
        while ((Vector2)transform.position != goal)
        {
            transform.position = Vector2.MoveTowards(transform.position, 
                goal, boltSpeed * Time.deltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(pause);
        ContinueMovement();
    }

    private void ContinueMovement()
    {
        _xDirectionMultiplier *= -1;
        _targetPosition += new Vector2(2 * _xDirectionMultiplier, 2);
        StartCoroutine(SegmentOfMovement());
        if (transform.position.y >= 10)
        {
            Destroy(gameObject);
        }
    }
    
    protected override void OnDestroy()
    {
        base.OnDestroy();
    }
}
