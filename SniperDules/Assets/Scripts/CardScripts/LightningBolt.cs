﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBolt : Card
{

    private GameObject _bolt;

    public override void Play()
    {
        _bolt = Instantiate(CardData.cardAsset, CastingPoint, Quaternion.identity);
        BoltBeh boltBeh = _bolt.GetComponent<BoltBeh>();
        boltBeh.myUnit = CastingPlayer;
        boltBeh.damage = CardData.damage;
        boltBeh.chosenTilePosition = ChosenTile.transform.position;
        boltBeh.StartMovement();
    }
    
    public override string GetDescription()
    {
        return string.Format(description, CardData.damage.ToString());
    }

    protected override void GetCreatableTiles()
    {
        foreach (var tile in TileManager.Instance.allTiles)
        {
            if (tile.transform.position.x == CastingPoint.x + 1 || tile.transform.position.x == CastingPoint.x - 1)
            {
                bool isTileInFrontOfCaster;
                if (CastingPlayer.team == Team.GreenTeam)
                {
                    isTileInFrontOfCaster = tile.transform.position.y == CastingPoint.y - 1;
                }
                else
                {
                    isTileInFrontOfCaster = tile.transform.position.y == CastingPoint.y + 1;
                }

                if (isTileInFrontOfCaster)
                {
                    tile.SetCreatable();
                }
            }
        }
    }
    
}
