﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public bool isWalkable;
    public bool isCreatable;
    private SpriteRenderer _renderer;
    private Color _highlightingColor;
    private void Start()
    {
        _highlightingColor = TileManager.Instance.highlightingColor;
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        if (isWalkable && !GameState.Instance.selectedUnit.hasMoved)
        {
            if (GameState.Instance.selectedUnit == GameState.Instance.currentPlayer)
            {
                GameState.Instance.currentPlayer.isSupportingCardPlayed = true; // Move to Player script.
                UiManager.Instance.DisableSupportingCards();
            }

            TileManager.Instance.CreateFuturePositionMark(this, GameState.Instance.selectedUnit.gameObject);
            GameState.Instance.ResetTilesAndUnits(); // Create Unit highlighting.
        }
        else if (isCreatable)
        {
            TileManager.Instance.ChoseTileForCardJustPlayed(this);
        }

    }

    public bool IsClear()
    {
        var obstacle = Physics2D.OverlapCircle(transform.position, 0.2F, TileManager.Instance.obstacleLayer);
        return obstacle == null;
    }
    
    public void SetCreatable()
    {
        Highlight(_highlightingColor);
        isCreatable = true;
    }
    
    public void SetWalkable()
    {     
        Highlight(_highlightingColor);
        isWalkable = true;
    }

    public void Highlight(Color highlightingColor)
    {
        _renderer.color = highlightingColor;
    }

    public void Reset()
    {
        _renderer.color = Color.white;
        isWalkable = false;
        isCreatable = false;
    }

}
