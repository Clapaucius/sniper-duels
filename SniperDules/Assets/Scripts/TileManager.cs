﻿using System;
using System.Collections.Generic;
using UnityEngine;


    public class TileManager : MonoBehaviour
    {

        public LayerMask obstacleLayer;
        public Color highlightingColor;
        [HideInInspector] public Tile[] allTiles;

        [Range(0, 1)] [SerializeField] private float alphaOfTransparentMark;
        private List<GameObject> _temporaryObjects = new List<GameObject>();
        private Card _cardJustPlayed;
        private PlayCard _playCard;
        
        public static TileManager Instance
        {
            get;
            private set;
        }

        private void Awake()
        {
            Instance = this;
            _playCard = FindObjectOfType<PlayCard>();
            allTiles = FindObjectsOfType<Tile>();
        }

        public void ChoseTileForCardJustPlayed(Tile chosenTile)
        {
            _cardJustPlayed.GetChosenTile(chosenTile);
            _playCard.PrepareCardToPlay(_cardJustPlayed);
            ResetAllTiles();
        }
        
        public void CreateFuturePositionMark(Tile tileForCreatingMark, GameObject objectForCreatingMark)
        {
            GameObject _futureUnitPositionMark = new GameObject();
            _temporaryObjects.Add(_futureUnitPositionMark);

            #region SpriteSetting
            _futureUnitPositionMark.transform.position = tileForCreatingMark.transform.position;
            SpriteRenderer newRenderer = _futureUnitPositionMark.AddComponent<SpriteRenderer>();
            SpriteRenderer oldRenderer = objectForCreatingMark.GetComponentInChildren<SpriteRenderer>();
            newRenderer.sprite = oldRenderer.sprite;
            Color tempColor = oldRenderer.color;
            newRenderer.sortingLayerID = oldRenderer.sortingLayerID;
            newRenderer.sortingOrder = oldRenderer.sortingOrder;
            newRenderer.maskInteraction = oldRenderer.maskInteraction;
            tempColor.a = alphaOfTransparentMark;
            newRenderer.color = tempColor;
            #endregion
            
            if (objectForCreatingMark.GetComponent<WalkingUnit>() != null)
            {
                objectForCreatingMark.GetComponent<WalkingUnit>().SetFuturePosition(tileForCreatingMark.transform.position);
            }
        }

        public void HighlightWalkableTiles(Vector2 unitPosition, int tileSpeed)
        {   
            foreach (var tile in allTiles)
            {
                if (Mathf.Abs(unitPosition.x - tile.transform.position.x) <= tileSpeed
                    && Mathf.Abs(unitPosition.y - tile.transform.position.y) <= tileSpeed)
                {
                    if (tile.IsClear() == true)
                    {
                        tile.SetWalkable();
                    }
                }
            }
        }

        public void ResetAllTiles()
        {
            //  DestroyAllMarks();
            _cardJustPlayed = null; 
            foreach (var tile in allTiles)
            {
                tile.Reset();    
            }
        }

        public void SetCardJustPlayed(Card cardJustPlayed)
        {
            _cardJustPlayed = cardJustPlayed;
        }
        
        public void DestroyAllMarks()
        {
            foreach (var temporaryObject in _temporaryObjects)
            {
                Destroy(temporaryObject);
            }
            _temporaryObjects.Clear();
        }
    }
