﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionSummoning : Card
{
    public override void Play()
    {
       GameObject minion = Instantiate(CardData.cardAsset, ChosenTile.transform.position, Quaternion.identity);
       minion.GetComponent<WalkingUnit>().team = CastingPlayer.team;
    }
    public override string GetDescription()
    {
        return string.Format(description, CardData.cardAsset.GetComponent<WalkingUnit>().Health.ToString(), 
            CardData.damage.ToString());
    }

    protected override void GetCreatableTiles()
    {
        float xDistance, yDistance;
        foreach (var tile in TileManager.Instance.allTiles)
        {
            xDistance = Mathf.Abs(tile.transform.position.x - CastingPoint.x);
            yDistance = Mathf.Abs(tile.transform.position.y - CastingPoint.y);
            if ((xDistance <= 1) && (yDistance <= 1) && tile.IsClear())
            {
                tile.SetCreatable();
            }
        }
    }
}
